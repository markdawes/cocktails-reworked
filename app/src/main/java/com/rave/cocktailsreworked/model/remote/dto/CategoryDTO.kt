package com.rave.cocktailsreworked.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDTO(
    val strCategory: String
)
