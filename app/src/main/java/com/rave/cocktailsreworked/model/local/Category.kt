package com.rave.cocktailsreworked.model.local

/**
 * Category.
 *
 * @property strCategory
 * @constructor Create empty Category
 */
data class Category(
    val strCategory: String
)
