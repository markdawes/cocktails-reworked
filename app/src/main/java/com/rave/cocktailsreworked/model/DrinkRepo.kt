package com.rave.cocktailsreworked.model

import com.rave.cocktailsreworked.model.local.Category
import com.rave.cocktailsreworked.model.local.CategoryDrink
import com.rave.cocktailsreworked.model.local.Drink
import com.rave.cocktailsreworked.model.remote.APIService
import com.rave.cocktailsreworked.model.remote.NetworkResponse
import com.rave.cocktailsreworked.model.remote.dto.CategoryResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

/**
 * Repository class to mediate how drink data is fetched.
 *
 * @property service
 * @constructor Create empty Drink repo
 */
class DrinkRepo @Inject constructor(private val service: APIService) {

    /**
     * Fetch drink categories.
     *
     * @return
     */
    suspend fun getDrinkCategories(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val response: Response<CategoryResponse> =
            service.getAllDrinkCategories()

        return@withContext if (response.isSuccessful) {
            val categoryResponse = response.body() ?: CategoryResponse()
            val categoryList: List<Category> = categoryResponse.drinks
                .map { Category(strCategory = it.strCategory) }
            NetworkResponse.Success.CategorySuccess(categoryList)
        } else {
            NetworkResponse.Error(response.message())
        }
    }

    /**
     * Get drinks from category.
     *
     * @param category
     * @return
     */
    suspend fun getDrinkInCategory(category: Category): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val categoryDrinkResponse = service.getDrinkFromCategory(category.strCategory)
            if (categoryDrinkResponse.isSuccessful) {
                val drinksInCategoryResponse =
                    categoryDrinkResponse.body()?.drinks ?: emptyList()
                NetworkResponse.Success.CategoryDrinkSuccess(
                    drinksInCategoryResponse.map {
                        CategoryDrink(
                            idDrink = it.idDrink,
                            strDrink = it.strDrink,
                            strDrinkThumb = it.strDrinkThumb
                        )
                    }
                )
            } else {
                NetworkResponse.Error(categoryDrinkResponse.message())
            }
        }

    /**
     * Get drink from id.
     *
     * @param id
     * @return
     */
    suspend fun getDrinkFromId(id: String): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val drinkResponse = service.getDrink(id)
            if (drinkResponse.isSuccessful) {
                val drinksInResponse =
                    drinkResponse.body()?.drinks ?: emptyList()
                NetworkResponse.Success.DrinkSuccess(
                    drinksInResponse.map {
                        Drink(
                            dateModified = it.dateModified,
                            idDrink = it.idDrink,
                            strAlcoholic = it.strAlcoholic,
                            strCategory = it.strCategory,
                            strCreativeCommonsConfirmed = it.strCreativeCommonsConfirmed,
                            strDrink = it.strDrink,
                            strDrinkAlternate = it.strDrinkAlternate,
                            strDrinkThumb = it.strDrinkThumb,
                            strGlass = it.strGlass,
                            strIBA = it.strIBA,
                            strImageAttribution = it.strImageAttribution,
                            strImageSource = it.strImageSource,
                            strTags = it.strTags,
                            strVideo = it.strVideo
                        )
                    }
                )
            } else {
                NetworkResponse.Error(drinkResponse.message())
            }
        }
}
