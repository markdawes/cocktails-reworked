package com.rave.cocktailsreworked.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
