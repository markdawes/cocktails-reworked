package com.rave.cocktailsreworked.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrinkResponse(
    val drinks: List<CategoryDrinkDTO>
)
