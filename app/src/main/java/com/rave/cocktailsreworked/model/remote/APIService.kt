package com.rave.cocktailsreworked.model.remote

import com.rave.cocktailsreworked.model.remote.dto.CategoryDrinkResponse
import com.rave.cocktailsreworked.model.remote.dto.CategoryResponse
import com.rave.cocktailsreworked.model.remote.dto.DrinkResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Service class that creates the endpoints needed.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getAllDrinkCategories(): Response<CategoryResponse>

    @GET(DRINK_IN_CATEGORY_ENDPOINT)
    suspend fun getDrinkFromCategory(@Query("c") categoryName: String): Response<CategoryDrinkResponse>

    @GET(DRINK_ENDPOINT)
    suspend fun getDrink(@Query("i") drinkId: String): Response<DrinkResponse>

    companion object {
        private const val CATEGORY_ENDPOINT = "list.php?c=list"
        private const val DRINK_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val DRINK_ENDPOINT = "lookup.php"
    }
}
