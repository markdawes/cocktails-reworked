package com.rave.cocktailsreworked.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
data class DrinkResponse(
    val drinks: List<DrinkDTO>
)
