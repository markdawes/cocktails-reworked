package com.rave.cocktailsreworked

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Custom Application class.
 *
 * @constructor Create empty Drink application
 */
@HiltAndroidApp
class DrinkApplication : Application()
