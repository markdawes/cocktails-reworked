package com.rave.cocktailsreworked.view.categoryscreen

import com.rave.cocktailsreworked.model.local.Category

/**
 * data class used to hold state for category screen.
 *
 * @property isLoading
 * @property categories
 * @constructor Create empty Category state
 */
data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList()
)
