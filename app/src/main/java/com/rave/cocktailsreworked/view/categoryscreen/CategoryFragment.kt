package com.rave.cocktailsreworked.view.categoryscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.cocktailsreworked.R
import com.rave.cocktailsreworked.model.local.Category
import com.rave.cocktailsreworked.ui.theme.CocktailsReworkedTheme
import com.rave.cocktailsreworked.viewmodel.DrinkViewModel

/**
 * Category fragment.
 *
 * @constructor Create empty Category fragment
 */
class CategoryFragment : Fragment() {

    private val viewModel: DrinkViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = viewModel.categoryState.collectAsState().value
                CocktailsReworkedTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        LazyColumn(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.SpaceEvenly,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            items(state.categories) { category: Category ->
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                    horizontalArrangement = Arrangement.SpaceAround,
                                    modifier = Modifier.clickable {
                                        viewModel.getDrinksInCategory(category)
                                        findNavController().navigate(
                                            R.id.action_categoryFragment_to_categoryDrinkFragment
                                        )
                                    }
                                ) {
                                    Text(
                                        text = category.strCategory,
                                        // modifier = Modifier.size(150.dp),
                                        style = TextStyle(
                                            fontWeight = FontWeight.SemiBold,
                                            fontSize = 30.sp
                                        )
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
