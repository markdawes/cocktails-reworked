package com.rave.cocktailsreworked.view.drinkscreen

import com.rave.cocktailsreworked.model.local.Drink

/**
 * Drink state.
 *
 * @property isLoading
 * @property drinks
 * @property error
 * @constructor Create empty Drink state
 */
data class DrinkState(
    val isLoading: Boolean = false,
    val drinks: List<Drink> = emptyList(),
    val error: String = "Error encountered"
)
