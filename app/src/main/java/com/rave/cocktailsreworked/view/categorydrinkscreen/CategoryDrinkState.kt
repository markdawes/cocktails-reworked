package com.rave.cocktailsreworked.view.categorydrinkscreen

import com.rave.cocktailsreworked.model.local.CategoryDrink

/**
 * Category drink state.
 *
 * @property isLoading
 * @property categoryDrinks
 * @property errMsg
 * @constructor Create empty Category drink state
 */
data class CategoryDrinkState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CategoryDrink> = emptyList(),
    val errMsg: String = "Error encountered"
)
