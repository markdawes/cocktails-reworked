package com.rave.cocktailsreworked.view.drinkscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import coil.compose.AsyncImage
import com.rave.cocktailsreworked.viewmodel.DrinkViewModel

/**
 * Fragment for drink screen.
 *
 * @constructor Create empty Drink fragment
 */
class DrinkFragment : Fragment() {

    private val viewModel: DrinkViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = viewModel.drinkState.collectAsState().value
                for (drink in state.drinks) {
                    Text(text = "Details for ${drink.strDrink}")
                    Text(text = "\nDrink is ${drink.strAlcoholic}")
                    Text(text = "\n\nDrink is in category ${drink.strCategory}")
                    AsyncImage(
                        model = "${drink.strImageSource}",
                        contentDescription = "\n\n\nImage for ${drink.strDrink}"
                    )
                }
            }
        }
    }
}
