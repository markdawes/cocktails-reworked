package com.rave.cocktailsreworked.view.categorydrinkscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import coil.compose.AsyncImage
import com.rave.cocktailsreworked.R
import com.rave.cocktailsreworked.model.local.CategoryDrink
import com.rave.cocktailsreworked.viewmodel.DrinkViewModel

/**
 * Category drink fragment.
 *
 * @constructor Create empty Category drink fragment
 */
class CategoryDrinkFragment : Fragment() {

    private val viewModel: DrinkViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = viewModel.categoryDrinkState.collectAsState().value
                LazyColumn() {
                    items(state.categoryDrinks) { drink: CategoryDrink ->
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceAround,
                            modifier = Modifier.clickable {
                                viewModel.getDrinkFromId(drink.idDrink)
                                findNavController().navigate(
                                    R.id.action_categoryDrinkFragment_to_drinkFragment
                                )
                            }
                        ) {
                            AsyncImage(
                                model = drink.strDrinkThumb,
                                contentDescription = "${drink.strDrink} shown as an image."
                            )
                            Text(text = drink.strDrink)
                        }
                    }
                }
            }
        }
    }
}
