package com.rave.cocktailsreworked.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rave.cocktailsreworked.R
import com.rave.cocktailsreworked.viewmodel.DrinkViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main entry point for application.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : FragmentActivity() {
    @Suppress("LateinitUsage")
    private lateinit var navController: NavController

    private val drinkViewModel by viewModels<DrinkViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        drinkViewModel.getCategories()
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }
}
