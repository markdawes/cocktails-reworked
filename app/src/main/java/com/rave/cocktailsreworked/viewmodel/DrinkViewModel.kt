package com.rave.cocktailsreworked.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.cocktailsreworked.model.DrinkRepo
import com.rave.cocktailsreworked.model.local.Category
import com.rave.cocktailsreworked.model.local.CategoryDrink
import com.rave.cocktailsreworked.model.local.Drink
import com.rave.cocktailsreworked.model.remote.NetworkResponse
import com.rave.cocktailsreworked.view.categorydrinkscreen.CategoryDrinkState
import com.rave.cocktailsreworked.view.categoryscreen.CategoryState
import com.rave.cocktailsreworked.view.drinkscreen.DrinkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel class to fetch and hold drink data for view.
 *
 * @property repo
 * @constructor Create empty Drink view model
 */
@HiltViewModel
class DrinkViewModel @Inject constructor(private val repo: DrinkRepo) : ViewModel() {

    private val _categoryState = MutableStateFlow(CategoryState())
    val categoryState get() = _categoryState.asStateFlow()

    private val _categoryDrinkState = MutableStateFlow(CategoryDrinkState())
    val categoryDrinkState get() = _categoryDrinkState.asStateFlow()

    private val _drinkState = MutableStateFlow(DrinkState())
    val drinkState get() = _drinkState.asStateFlow()

    /**
     * Fetch categories and update [CategoryState] with response.
     *
     */
    fun getCategories() = viewModelScope.launch {
        _categoryState.update { it.copy(isLoading = true) }
        val categories = repo.getDrinkCategories()
        handleResults(categories)
    }

    /**
     * Get drinks in category.
     *
     * @param category
     */
    fun getDrinksInCategory(category: Category) = viewModelScope.launch {
        _categoryDrinkState.update { it.copy(isLoading = true) }
        val categoryDrinks = repo.getDrinkInCategory(category)
        handleResults(categoryDrinks)
    }

    /**
     * Get drink from id.
     *
     * @param id
     */
    fun getDrinkFromId(id: String) = viewModelScope.launch {
        _drinkState.update { it.copy(isLoading = true) }
        val drinks = repo.getDrinkFromId(id)
        handleResults(drinks)
    }

    private fun handleResults(networkResponse: NetworkResponse<*>) {
        when (networkResponse) {
            is NetworkResponse.Error -> _categoryState.update {
                it.copy(
                    isLoading = false
                )
            }
            is NetworkResponse.Success.CategorySuccess<*> -> _categoryState.update {
                it.copy(
                    isLoading = false,
                    categories = networkResponse.categoryList as List<Category>
                )
            }
            is NetworkResponse.Success.CategoryDrinkSuccess<*> -> _categoryDrinkState.update {
                it.copy(
                    isLoading = false,
                    categoryDrinks = networkResponse.drinksInCategory as List<CategoryDrink>
                )
            }
            is NetworkResponse.Success.DrinkSuccess<*> -> _drinkState.update {
                it.copy(
                    isLoading = false,
                    drinks = networkResponse.drinks as List<Drink>
                )
            }
            else -> Log.e("tag", "Error encountered")
        }
    }
}
