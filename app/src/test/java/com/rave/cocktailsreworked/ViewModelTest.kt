package com.rave.cocktailsreworked

import com.rave.cocktailsreworked.model.DrinkRepo
import com.rave.cocktailsreworked.model.local.Category
import com.rave.cocktailsreworked.model.remote.NetworkResponse
import com.rave.cocktailsreworked.viewmodel.DrinkViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

/**
 * View model test.
 *
 * @constructor Create empty View model test
 */
@OptIn(ExperimentalCoroutinesApi::class)
internal class ViewModelTest {

    @RegisterExtension
    private val extension = CoroutinesTestExtension()

    private val repo = mockk<DrinkRepo>()

    private val viewModel: DrinkViewModel = DrinkViewModel(repo)

    /**
     * Test view model state.
     *
     */
    @Test
    @DisplayName("Testing ViewModel State")
    fun testViewModelState() = runTest(extension.dispatcher) {
        val expectedResponse = NetworkResponse.Success.CategorySuccess(
            listOf(
                Category(
                    strCategory = "Sports Drink"
                )
            )
        )

        // Given
        coEvery { repo.getDrinkCategories() } coAnswers { expectedResponse }
        // When
        val categoryResponse = viewModel.getCategories()

        // Then
        Assertions.assertNotEquals(null, categoryResponse)
        Assertions.assertTrue(categoryResponse is Job)
    }
}
