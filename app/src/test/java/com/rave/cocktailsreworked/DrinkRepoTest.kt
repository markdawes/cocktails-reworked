package com.rave.cocktailsreworked

import com.rave.cocktailsreworked.model.DrinkRepo
import com.rave.cocktailsreworked.model.local.Category
import com.rave.cocktailsreworked.model.local.CategoryDrink
import com.rave.cocktailsreworked.model.remote.APIService
import com.rave.cocktailsreworked.model.remote.dto.CategoryDTO
import com.rave.cocktailsreworked.model.remote.dto.CategoryDrinkDTO
import com.rave.cocktailsreworked.model.remote.dto.CategoryDrinkResponse
import com.rave.cocktailsreworked.model.remote.dto.CategoryResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
internal class DrinkRepoTest {

    @RegisterExtension
    private val extension = CoroutinesTestExtension()
    private val mockService = mockk<APIService>()
    private val repo = DrinkRepo(mockService)

    @Test
    @DisplayName("Testing getting all the drink categories")
    fun testGetCategories() = runTest(extension.dispatcher) {
        // Given
        val response = CategoryResponse(
            listOf(CategoryDTO("Testing"))
        )
        val result = response.drinks.map {
            Category(
                strCategory = it.strCategory
            )
        }

        coEvery { mockService.getAllDrinkCategories() } coAnswers { Response.success(response) }

        // When
        val testResult = repo.getDrinkCategories()

        // Then
        Assertions.assertEquals(result, testResult)
    }

    @Test
    @DisplayName("Testing getting drinks in category")
    fun testGetCategoryDrinks() = runTest(extension.dispatcher) {
        // Given
        val response = CategoryDrinkResponse(
            listOf(
                CategoryDrinkDTO("testing", "testrepo.com", "hello")
            )
        )
        val result = response.drinks.map {
            CategoryDrink(
                idDrink = it.idDrink,
                strDrink = it.strDrink,
                strDrinkThumb = it.strDrinkThumb
            )
        }

        coEvery { mockService.getDrinkFromCategory("testing") } coAnswers { Response.success(response) }

        // When
        val testResult = repo.getDrinkInCategory(Category("testing"))

        // Then
        Assertions.assertEquals(Response.success(result), testResult)
    }

    @Test
    @DisplayName("Testing getting drink from id")
    fun testGetDrinks() = runTest(extension.dispatcher) {
        // Given
        /*val response = DrinkResponse(
            listOf(
                DrinkDTO(
                    "1",
                    "test",
                    "test",
                    "test.com",
                    strIngredient1 = "test",
                    strMeasure1 = "test"
                )
            )
        )
        coEvery { mockService.getDrink("test") } coAnswers { Response.success(response) }

        // When
        val testResult = repo.getDrinkFromId("test")

        // Then
        Assertions.assertEquals(result, testResult)*/
    }
}
